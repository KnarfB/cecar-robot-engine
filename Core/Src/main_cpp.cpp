/*
 * main.cpp
 *
 *      Copyright (c) 2019, Frank Bauernoeppel
 *
 *      TwistStampedMsg related computations and message handling derived from
 *      https://gitlab.com/NeuroRace/neuroracer-robot-engine/
 *
 *      Parts Copyright (c) 2016, JetsonHacks
 *      Parts Copyright (c) 2017, Timm Fröhlich
 *      Parts Copyright (c) 2018, HTW Berlin, Patrick Baumann
 *
 *      rosserial_stm32 implementation derived from
 *      https://github.com/yoneken/rosserial_stm32/
 *      Copyright (c) 2018, Kenta Yonekura
 *
 */

#include <stdio.h>

#include <main.h>
#include <main_cpp.h>

#include <ros.h>
#include <std_msgs/Header.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/TwistStamped.h>

#include <stm32f4_discovery_accelerometer.h>

constexpr double pi = 3.14159265358979323846;

//////////////////////////////////////////////////////

ros::NodeHandle nh;

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	nh.getHardware()->flush();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	nh.getHardware()->reset_rbuf();
}

///////////////////////////////////////////////////////

// at the moment, we follow neuroracer's ROS message semantics
// TODO: switch to AckermannDriveStamped messaging
// http://docs.ros.org/api/ackermann_msgs/html/msg/AckermannDrive.html

//////////////////////////////////////////////////////////////////

// 500 (=0.5 µs) is 100%
const int diffSteer_cecar1 		= 450;
const int diffThrottle_cecar1 	= 200;


// range of incoming /nr/engine/input/actions messages
const double boundary_low 	= -1.0;
const double boundary_high 	= +1.0;


// General bounds for the steering servo and the ESC
// timer htim3 runs @ 1 MHz, pulse width in µs in range [1ms .. 2ms]:
const int servoNeutral 		= 1500;

// ROS parameter
int usSteerRange 			= diffSteer_cecar1;
int usThrottleRange 		= diffThrottle_cecar1;

const int minSteering 		= servoNeutral - usSteerRange;
const int maxSteering 		= servoNeutral + usSteerRange;

const int minThrottle 		= servoNeutral - usThrottleRange;
const int maxThrottle 		= servoNeutral + usThrottleRange;

/**
 * Arduino 'map' funtion for floating point
 */
static double fmap (double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
 * Combined MIN and MAX.
 */
inline int between(int x, int minimum, int maximum) {
  return x < minimum ? minimum : (x > maximum ? maximum : x);
}

/**
 * Map steering input to a valid servo angle.
 *
 * @param double steer Value from boundary_low to boundary_high
 * (its the same value but could also be different from mapThrottle)
 * @return int Steering angle
 */
int mapSteering(double steer) {
  int servoAngle = (int)fmap(steer, boundary_low, boundary_high, minSteering, maxSteering);
  return between(servoAngle, minSteering, maxSteering);
}

/**
 * Map throttle input to a valid servo angle for the ESC.
 *
 * @param double throttle Value from boundary_low to boundary_high
 * (its the same value but could also be different from mapSteering)
 * @return int Throttle angle / value
 */
int mapThrottle(double throttle) {
  int servoAngle = (int)fmap(throttle, boundary_low, boundary_high, minThrottle, maxThrottle);
  return between(servoAngle, minThrottle, maxThrottle);
}

extern TIM_HandleTypeDef htim3;

void driveCallback(const geometry_msgs::TwistStamped&  TwistStampedMsg) {
   const double steerIn = TwistStampedMsg.twist.angular.z;
   const double throttleIn = TwistStampedMsg.twist.linear.x;
   const int steer = mapSteering(steerIn);
   const int throttle = mapThrottle(throttleIn);

   PRINTF("PWM: steer: %4d; throttle: %4d\n", steer, throttle );
   __HAL_TIM_SET_COMPARE( &htim3, TIM_CHANNEL_1, steer );
   __HAL_TIM_SET_COMPARE( &htim3, TIM_CHANNEL_2, throttle );
}

ros::Subscriber<geometry_msgs::TwistStamped> driveSubscriber( "/nr/engine/input/actions", &driveCallback);

std_msgs::Float32 speed_rl_msg;
ros::Publisher speed_rl_pub("/nr/tachometer/output/speed/rl", &speed_rl_msg);

std_msgs::Float32 speed_rr_msg;
ros::Publisher speed_rr_pub("/nr/tachometer/output/speed/rr", &speed_rr_msg);

//////////////////////////////////////////////////////////////////

// wheel encoders via EXIT

uint32_t wenc_tacho[4];

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if( GPIO_Pin==WENC_TACHO_FL_Pin) {
		++wenc_tacho[0];
	}
	if( GPIO_Pin==WENC_TACHO_FR_Pin) {
		++wenc_tacho[1];
	}
	if( GPIO_Pin==WENC_TACHO_RL_Pin) {
		++wenc_tacho[2];
	}
	if( GPIO_Pin==WENC_TACHO_RR_Pin) {
		++wenc_tacho[3];
	}
}

void harvest_wheel_encoders()
{
	static uint32_t last_wenc_tacho[4];
	float slots[4];
	for( int i=0; i<4; ++i ) {
		slots[i] = wenc_tacho[i] - last_wenc_tacho[i];
		last_wenc_tacho[i] = wenc_tacho[i];
	}

	// Two HOA902 wheel encoders are installed on rear wheels.
	// Each encoder disk has 60 slots.
	const float slots_per_revolution = 60;

	//	Wheel diameter is 83.5mm --> circumference 262.3mm.
	const float wheel_circumference = 3.14 * 0.0835; // m

	// measurement period is 100 ms
	const float measurement_period = 0.1; // s

	float speed[4];
	for( int i=0; i<4; ++i ) {
		speed[i] = ((slots[i] / measurement_period) / slots_per_revolution) * wheel_circumference;
	}
	// Target speed 0.1 m/s --> 0.381 wheel rotations/s --> 22.86 slots/s --> 45.72 tacho pulses/s
	// Target speed 1.0 m/s --> 3.81 wheel rotations/s --> 228.6 slots/s --> 457.2 tacho pulses/s
	// Target speed 10.0 m/s --> 38.1 wheel rotations/s --> 2286 slots/s --> 4572 tacho pulses/s

	// change signs depending on WHHEL_ENC_DIR GPIOs
	if( HAL_GPIO_ReadPin( WENC_DIR_RL_GPIO_Port, WENC_DIR_RL_Pin) == GPIO_PIN_RESET) {
		speed[0] = -speed[0];
	}
	if( HAL_GPIO_ReadPin( WENC_DIR_RR_GPIO_Port, WENC_DIR_RR_Pin) == GPIO_PIN_SET) {
		speed[1] = -speed[1];
	}
	if( HAL_GPIO_ReadPin( WENC_DIR_RL_GPIO_Port, WENC_DIR_RL_Pin) == GPIO_PIN_RESET) {
		speed[2] = -speed[2];
	}
	if( HAL_GPIO_ReadPin( WENC_DIR_RR_GPIO_Port, WENC_DIR_RR_Pin) == GPIO_PIN_SET) {
		speed[3] = -speed[3];
	}

	speed_rl_msg.data = speed[2];
	speed_rl_pub.publish( &speed_rl_msg );

	speed_rr_msg.data = speed[3];
	speed_rr_pub.publish( &speed_rr_msg );

	PRINTF("speed rear axle: rl=%8.2f rr=%8.2f\n", speed[2], speed[3] );

	int16_t dataXYZ[3];
	BSP_ACCELERO_GetXYZ(dataXYZ); // data in milli g
	// X: planar==0; positive value == left side down
	// Y: planar==0; positive value == front goes up
	// Z: planar=1000; positive value is down
	PRINTF("accelero X=%6d Y=%6d Z=%6d\n", dataXYZ[0], dataXYZ[1], dataXYZ[2] );
}

// wheel encoder timers
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim4;

// wheel encoder periodic interrupt
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim==&htim2) {
		harvest_wheel_encoders(); // 100ms
	}
}

void setup()
{
	PRINTF("begin setup\n");
	// servo PWM
	__HAL_TIM_SET_COMPARE( &htim3, TIM_CHANNEL_1, servoNeutral );
    __HAL_TIM_SET_COMPARE( &htim3, TIM_CHANNEL_2, servoNeutral );

    // PWM timer for the servos: steer, throttle
	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);

	nh.initNode();
	ros::Time begin = nh.now(); // see http://wiki.ros.org/rosserial/Overview/Time

	nh.loginfo("Hello from rosserial client");  // see http://wiki.ros.org/rosserial/Overview/Logging

	nh.subscribe(driveSubscriber);
	nh.advertise( speed_rl_pub );
	nh.advertise( speed_rr_pub );

	// use hard coded defaults
	usSteerRange = diffSteer_cecar1;
	usThrottleRange = diffThrottle_cecar1;

	// clock source for wheel encoder
	HAL_TIM_Base_Start_IT(&htim2);

	BSP_ACCELERO_Init();
	uint8_t accelero_id = BSP_ACCELERO_ReadID();
	PRINTF("found accelero id %d\n", accelero_id );
	PRINTF("end setup\n");
}

void loop()
{
	static uint32_t count;
	static uint32_t not_connected_count;

	nh.spinOnce();
	count++;

	if( nh.connected() ) {
		not_connected_count = 0;
		if( (count % 100)==0 ) {
			HAL_GPIO_WritePin( LED_OR_GPIO_Port, LED_OR_Pin, GPIO_PIN_RESET );
			HAL_GPIO_TogglePin( LED_GN_GPIO_Port, LED_GN_Pin );
		}
	} else {
		not_connected_count++;
		if( (count % 20)==0 ) {
			HAL_GPIO_WritePin( LED_GN_GPIO_Port, LED_GN_Pin, GPIO_PIN_RESET );
			HAL_GPIO_TogglePin( LED_OR_GPIO_Port, LED_OR_Pin );
		}
	}

	if(not_connected_count > 1000) {
		PRINTF("not connected - perform reset\n" );
		HAL_GPIO_WritePin( LED_RD_GPIO_Port, LED_RD_Pin, GPIO_PIN_SET );
		HAL_Delay(100);
		HAL_NVIC_SystemReset();
	}
	HAL_Delay(1);
}
